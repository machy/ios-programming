//
//  ConversionViewController.swift
//  WorldTrotter
//
//  Created by Chongyang Ma on 1/7/17.
//  Copyright © 2017 Chongyang Ma. All rights reserved.
//

import UIKit

class ConversionViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var celsiusLabel: UILabel!
    @IBOutlet var textField: UITextField!

    var fahrenheitValue: Measurement<UnitTemperature>? {
        didSet {
            updateCelsiusLabel()
        }
    }

    var celsiusValue: Measurement<UnitTemperature>? {
        if let fahrenheitValue = fahrenheitValue {
            return fahrenheitValue.converted(to: .celsius)
        } else {
            return nil
        }
    }

    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 1
        return nf
    }()

    func updateCelsiusLabel() {
        if let celsiusValue = celsiusValue {
            celsiusLabel.text = numberFormatter.string(from: NSNumber(value: celsiusValue.value))
        } else {
            celsiusLabel.text = "???"
        }
    }

    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        textField.resignFirstResponder()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        // Get current string as `NSString`
        guard let existingString = textField.text as NSString? else {
            return false
        }
        // Perform replacement
        let newString = existingString.replacingCharacters(in: range, with: string)
        // Check for empty result
        if newString.isEmpty || newString == "-" {
            return true
        }
        // Check for valid number
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        return nf.number(from: newString) != nil
    }

    @IBAction func fahrenheitFieldEditingChanged(_ textField: UITextField) {

        if let text = textField.text, let number = numberFormatter.number(from: text) {
            fahrenheitValue = Measurement(value: number.doubleValue, unit: .fahrenheit)
        } else {
            fahrenheitValue = nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        print("ConversionViewController loaded its view.")

        updateCelsiusLabel()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let date = Date()
        let hour = Calendar.current.component(.hour, from: date)

        if hour > 17 || hour < 6 {
            self.view.backgroundColor = UIColor.darkGray
        }
    }
}
