//
//  Line.swift
//  TouchTracker
//
//  Created by Chongyang Ma on 2/17/17.
//  Copyright © 2017 Chongyang Ma. All rights reserved.
//

import Foundation
import CoreGraphics

struct Line {
    var begin = CGPoint.zero
    var end = CGPoint.zero
}
