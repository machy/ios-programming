iOS Programming
===============

Programming practice for the book [iOS Programming: The Big Nerd Ranch Guide (6th Edition)](https://www.bignerdranch.com/books/ios-programming/)

- Quiz (Chap 1, 8)
- WorldTrotter (Chap 3 - 7)
    * Views and the View Hierarchy
    * Text Input and Delegation
    * View Controllers
    * Programmatic Views
    * Localization
- Homepwner (Chap 10 - 17)
    * UITableView and UITableViewController
    * Editing UITableView
    * Subclassing UITableViewCell
    * Stack Views
    * UINavigationController
    * Camera
    * Saving, Loading and Application States
    * Size classes
- TouchTracker (Chap 18 - 19)
    * Touch Events and UIResponder
    * UIGestureRecognizer and UIMenuController
- Photorama (Chap 20 - 23)
    * Web Services
    * Collection Views
    * Core Data
    * Core Data Relationships
